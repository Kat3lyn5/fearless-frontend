window.addEventListener('DOMContentLoaded', async () => {
    //^add event listener for when the DOM loads
    const url = 'http://localhost:8000/api/locations/';
    //^ declare a variable that will hold the URL for the API created

    const response = await fetch(url);
    //Fetch URL..dont forget await keyword so we get response not promise.

    if (response.ok) {
      const data = await response.json();
      console.log(data);

      //if response if okay, get data using the .json method. dont forget await.
      const selectTag = document.getElementById('location');
      //Get the select tag element by its id 'location'

      for(let location of data.locations) {
        //for each location in the location property of the data

        const option = document.createElement('option');
        option.value = location.id;

        option.innerHTML = location.name;
        //set innerHTML of option element to conference's name.

        selectTag.appendChild(option);
        //append option element as child of select tag.
      }
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit',async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference);
      }
    });
  })
