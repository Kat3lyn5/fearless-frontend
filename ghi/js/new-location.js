window.addEventListener('DOMContentLoaded', async () => {
    //^add event listener for when the DOM loads
    const url = 'http://localhost:8000/api/states/';
    //^ declare a variable that will hold the URL for the API created

    const response = await fetch(url);
    //Fetch URL..dont forget await keyword so we get response not promise.

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      //if response if okay, get data using the .json method. dont forget await.

      const selectTag = document.getElementById('state');
      //Get the select tag element by its id 'state'

      for(let state of data.states) {
        //for each sate in the states property of the data

        let option = document.createElement('option');
        option.value = state.abbreviation;

        option.innerHTML = state.name;
        //set innerHTML of option element to state's name.

        selectTag.appendChild(option);
        //append option element as child of select tag.
      }
    }
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit',async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
        console.log(newLocation);
      }
    });
  })
